# Algorithmen und Datenstrukturen I, WiSe 2022-23 #

Der Inhalt dieses Repository ist keineswegs verpflichtend, sondern dient dem Zweck, „Handnotizen“ anzubieten.
Es besteht hier kein Anspruch auf Vollständigkeit.
Es werden hier keine offiziellen Lösungen für die Übungsblätter gespeichert.

Die offiziellen Notizen zum Kurs sowie alle Übungsblätter, (Abgabe)termine, _etc._
findet man ausschließlich auf der **Moodle**-Seite.

## (Wesentliche) Struktur des Repositorys ##

```text
.
├── ./notebooks # Python/Jupyter Notebooks
│   ├── ...
│   └── ... *.ipynb Dateien
├── ./src       # Quellcode
│   ├── ...
│   └── ... *.py Dateien
├── ./templates # Templates für Konfiguration
│   ├── ...
│   └── ... *.yaml Dateien
├── ./setup     # User Konfiguration
│   ├── ...
│   └── ... *.yaml Dateien
├── ./tests     # enthält ggf. automatisiertes Testing
├── ./dist      # enthält VERSION infos
├── ./models    # erzeugte Modelle
├── ./docs      # Dokumentation für erzeugte Modelle
│
├── justfile
├── README.md
├── LICENSE
├── requirements.txt
└── main.py
```

## Gebrauchshinweise ##

### Systemvoraussetzungen ###

- [**python 3.10.x**](https://www.python.org/downloads)
- optional: **bash** (für Windowsuser siehe [git for windows](https://gitforwindows.org), was bash mit installiert)
- optional: das [**justfile** Tool](https://github.com/casey/just#installation) (für Windowsuser wird die das [Chocolatey](https://chocolatey.org/install) Tool empfohlen, um dieses Tool installieren zu können)

### Setup ###

Für den Gebrauch der Python-Skripte + Notebooks braucht man einige Packages.
Um diese zu installiere führe man
```bash
just build
# oder
just build-requirements
```
aus. Alternativ kann man
```bash
python3 -m pip install -r --disable-pip-version-check -r requirements.txt
# für Windowsuser:
py -3 -m pip install -r --disable-pip-version-check -r requirements.txt
```
verwenden.

### Ausführung ###

Die Notebooks kann man direkt in dem [**jupyter**](https://jupyter.org) GUI öffnen,
oder in einem bash Terminal führe man
```bash
just notebook xyz
```
aus, um den Notebook **notebooks/xyz.ipynb** zu starten.
Alternativ, kann man
``` bash
python3 -m jupyter notebook notebooks/xyz.ipynb
# für Windowsuser:
py -3 -m jupyter notebook notebooks/xyz.ipynb
```
verwenden.
